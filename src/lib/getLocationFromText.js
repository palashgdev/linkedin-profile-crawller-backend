const cities = require("all-the-cities");
const countries = require("i18n-iso-countries");

const getIsCity = (text) => {
  const lowerCaseText = text.toLowerCase();
  return !!cities.find((city) => city.name.toLowerCase() === lowerCaseText);
};

const getIsCountry = (text) => {
  const countriesList = Object.values(countries.getNames("en"));
  const lowerCaseText = text.toLowerCase();

  return !!countriesList.find(
    (country) => country.toString().toLowerCase() === lowerCaseText
  );
};

/**
 * @function getLocationFromText
 * @param {String} text
 * @returns Object
 */
const getLocationFromText = (text) => {
  if (!text) {
    return null;
  }

  const cleanText = text.replace(" Area", "").trim();
  const parts = cleanText.split(", ");

  let city = null;
  let province = null;
  let country = null;

  if (parts.length === 3) {
    city = parts[0];
    province = parts[1];
    country = parts[2];

    return {
      city,
      province,
      country,
    };
  }

  if (parts.length === 2) {
    if (getIsCity(parts[0]) && getIsCountry(parts[1])) {
      return {
        city: parts[0],
        province,
        country: parts[1],
      };
    }

    if (getIsCity(parts[0]) && !getIsCountry(parts[1])) {
      return {
        city: parts[0],
        province: parts[1],
        country,
      };
    }

    return {
      city,
      province: parts[0],
      country: parts[1],
    };
  }

  if (getIsCountry(parts[0])) {
    return {
      city,
      province,
      country: parts[0],
    };
  }

  if (getIsCity(parts[0])) {
    return {
      city: parts[0],
      province,
      country,
    };
  }

  return {
    city,
    province: parts[0],
    country,
  };
};

module.exports = getLocationFromText;
