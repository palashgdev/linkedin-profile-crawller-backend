const moment = require("moment-timezone");

const getDurationInDays = (formattedStartDate, formattedEndDate) => {
  if (!formattedStartDate || !formattedEndDate) {
    return null;
  }
  return moment(formattedEndDate).diff(moment(formattedStartDate), "days") + 1;
};

module.exports = getDurationInDays;
