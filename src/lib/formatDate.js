const moment = require("moment-timezone");

const formatDate = (date) => {
  if (date === "Present") {
    return moment().format();
  }

  return moment(date, "MMMY").format();
};

module.exports = formatDate;
