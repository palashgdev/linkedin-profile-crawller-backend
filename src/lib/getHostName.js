const getHostname = (url) => {
  return new URL(url).hostname;
};

module.exports = getHostname;
