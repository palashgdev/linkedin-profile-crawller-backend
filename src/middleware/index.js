const profileApi = require("../routes");
const morgan = require("morgan");
const express = require("express");
const cors = require("cors");

const middleware = (app) => {
  app.use(cors());
  app.use(express.json());
  app.use(morgan("combined"));

  app.use("/api/profile", profileApi);
};

module.exports = middleware;
