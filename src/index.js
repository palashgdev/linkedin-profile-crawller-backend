const express = require("express");
const dotenv = require("dotenv");
const app = express();
const path = require("path");
const middleware = require("./middleware");

dotenv.config({
  path: path.resolve(__dirname, "..", ".env"),
});

const { PORT } = require("./config");

middleware(app);

app.listen(PORT, () => {
  console.log(`Server is started at ${PORT}`);
});
