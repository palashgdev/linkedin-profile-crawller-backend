const assert = require("assert");

const Assert = () => {
  const { PORT } = process.env;

  assert(PORT, "PORT variable is not present");
};

module.exports = Assert;
