const yup = require("yup");
const setUpPage = require("../functions/setUpPage");
const checkLoggedIn = require("../functions/checkLogin");
const autoScroll = require("../../lib/autoScroll");

const getProfileDate = require("../functions/getProfileData");
const getEducation = require("../functions/getEducation");
const getExperience = require("../functions/getExperience");
const getSkills = require("../functions/getSkills");
const getVolunteer = require("../functions/getVolunteer");

const formatDate = require("../../lib/formatDate");
const getDurationInDays = require("../../lib/getDurationInDays");
const getLocationFromText = require("../../lib/getLocationFromText");

const sanitizeData = require("../../lib/sanitizeData");

const getProfile = async (req, res) => {
  try {
    const {
      keepAlive,
      sessionCookie,
      timeout,
      userAgent,
      headless,
      linkedinUrl,
    } = req.body;

    const schema = yup.object().shape({
      keepAlive: yup.bool().strict().label("keepLive"),
      sessionCookie: yup
        .string()
        .label("sessionCookie")
        .required("sessionCookie is required as string")
        .min(1),
      timeout: yup
        .number()
        .integer()
        .positive()
        .label("timeout is required as positive integer"),
      userAgent: yup.string().label("userAgent").optional(),
      headless: yup.boolean().label("headless").strict().optional(),
      linkedinUrl: yup.string().label("linkedinUrl").min(1).strict().required(),
    });

    await schema.validate({
      keepAlive,
      sessionCookie,
      timeout,
      userAgent,
      headless,
      linkedinUrl,
    });

    // this will set the cookie and return the page
    const { page, browser } = await setUpPage({
      headless,
      timeout,
      userAgent,
      sessionCookie,
    });

    // console.log(page, "page");

    // const isUserLoggedIn = await checkLoggedIn({
    //   page,
    //   timeout: timeout * 2,
    // });

    // console.log(isUserLoggedIn, "isLoggedIn");

    await page.goto(linkedinUrl, {
      waitUntil: "networkidle2",
      timeout: timeout * 2,
    });

    await autoScroll({
      page,
    });

    // Only click the expanding buttons when they exist
    const expandButtonsSelectors = [
      ".lt-line-clamp__ellipsis > a", // About
      "#experience-section .pv-profile-section__see-more-inline.link", // Experience
      ".pv-profile-section.education-section button.pv-profile-section__see-more-inline", // Education
      '.pv-skill-categories-section [data-control-name="skill_details"]', // Skills
    ];

    // const seeMoreButtonsSelectors = [
    //   ".pv-deferred-area__content .pv-profile-section__see-more-inline",
    // ];

    for (const buttonSelector of expandButtonsSelectors) {
      //check if present or not
      if ((await page.$(buttonSelector)) !== null) {
        await page.click(buttonSelector);
      }
    }

    // wait for 200ms
    await page.waitFor(200);

    // for (const button of seeMoreButtonsSelectors) {
    //   if ((await page.$(button)) !== null) {
    //     await page.click(button);
    //   }
    // }

    const { payload: profilePayload } = await getProfileDate({ page });

    const {
      fullName,
      title,
      location,
      photo,
      description,
      url,
    } = profilePayload;

    const { payload: experiencePayload } = await getExperience({ page });

    const experiences = experiencePayload.map((experience) => {
      const startDate = formatDate(experience.startDate);
      const endDate = formatDate(experience.endDate) || null;
      const endDateIsPresent = experience.endDateIsPresent;

      const durationInDaysWithEndDate =
        startDate && endDate && !endDateIsPresent
          ? getDurationInDays(startDate, endDate)
          : null;
      const durationInDaysForPresentDate =
        endDateIsPresent && startDate
          ? getDurationInDays(startDate, new Date())
          : null;
      const durationInDays = endDateIsPresent
        ? durationInDaysForPresentDate
        : durationInDaysWithEndDate;

      return {
        ...experience,
        title: sanitizeData(experience.title),
        company: sanitizeData(experience.company),
        employmentType: sanitizeData(experience.employmentType),
        location: (experience || {}).location
          ? getLocationFromText(experience.location)
          : null,
        startDate,
        endDate,
        durationInDays,
        description: sanitizeData(experience.description),
      };
    });

    const { payload: EducationPayload } = await getEducation({ page });

    const education = EducationPayload.map((education) => {
      const startDate = formatDate(education.startDate);
      const endDate = formatDate(education.endDate);

      return {
        ...education,
        schoolName: sanitizeData(education.schoolName),
        degreeName: sanitizeData(education.degreeName),
        fieldOfStudy: sanitizeData(education.fieldOfStudy),
        startDate,
        endDate,
        durationInDays: getDurationInDays(startDate, endDate),
      };
    });

    const { payload: skillsPayload } = await getSkills({ page });

    const { payload: volunteerPayload } = await getVolunteer({ page });

    const volunteer = volunteerPayload.map((volunteer) => {
      const startDate = formatDate(volunteer.startDate);
      const endDate = formatDate(volunteer.endDate);

      return {
        ...volunteer,
        title: sanitizeData(volunteer.title),
        company: sanitizeData(volunteer.company),
        description: sanitizeData(volunteer.description),
        startDate,
        endDate,
        durationInDays: getDurationInDays(startDate, endDate),
      };
    });

    // will check this one
    try {
      if (!keepAlive) {
        if (page) {
          await page.close();
        }
        if (browser) {
          browser.close();
        }
      } else {
        await page.close();
      }
    } catch (error) {
      console.error(error);
    }

    res.json({
      statusCode: 200,
      message: "Success",
      payload: {
        profile: {
          fullName: sanitizeData(fullName),
          title: sanitizeData(title),
          location: sanitizeData(location),
          photo: sanitizeData(photo),
          description: sanitizeData(description),
          url: sanitizeData(url),
        },
        education,
        skills: skillsPayload,
        volunteer,
        experiences,
      },
    });
  } catch (error) {
    console.error(error);
    res.json({
      statusCode: 500,
      error,
    });
  }
};

module.exports = getProfile;
