const express = require("express");

const route = express.Router();
const getProfile = require("./getProfile");
const sendRequest = require("./sendRequest");

route.post("/", getProfile);
route.post("/sendRequest", sendRequest);

module.exports = route;
