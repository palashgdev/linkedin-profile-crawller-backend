const yup = require("yup");
const setupPage = require("../functions/setUpPage");

const sendRequest = async (req, res) => {
  try {
    const {
      linkedinProfileUrl,
      headless,
      timeout,
      sessionCookie,
      connectionMessage,
      keepAlive,
      userAgent,
    } = req.body;

    const schema = yup.object().shape({
      linkedinProfileUrl: yup
        .string()
        .strict()
        .required()
        .label("linkedinProfileUrl"),
      keepAlive: yup.bool().strict().label("keepLive"),
      sessionCookie: yup
        .string()
        .label("sessionCookie")
        .required("sessionCookie is required as string")
        .min(1),
      timeout: yup
        .number()
        .integer()
        .positive()
        .label("timeout is required as positive integer"),
      userAgent: yup.string().label("userAgent").optional(),
      headless: yup.boolean().label("headless").strict().optional(),
      connectionMessage: yup
        .string()
        .required()
        .strict()
        .label("connectionMessage"),
    });

    await schema.validate({
      linkedinProfileUrl,
      keepAlive,
      sessionCookie,
      timeout,
      userAgent,
      headless,
      connectionMessage,
    });

    const { page, browser } = await setupPage({
      headless,
      timeout,
      sessionCookie,
      userAgent,
    });

    await page.goto(linkedinProfileUrl, {
      waitUntil: "networkidle2",
      timeout: timeout * 2,
    });

    // await page.close();

    const connectButton = await page.$(
      ".pv-top-card .pv-s-profile-actions--connect"
    );

    if (connectButton != null) {
      connectButton.click();

      await page.waitFor(2000);

      //
      const addNoteButton = await page.$(
        '#artdeco-modal-outlet button.artdeco-button--muted[aria-label="Add a note"]'
      );

      if (addNoteButton != null) {
        addNoteButton.click();

        await page.waitFor(500);

        await page.type("textarea", connectionMessage, {
          delay: 10,
        });

        const sendInvitation = await page.$(
          'button[aria-label="Send invitation"]'
        );

        if (sendInvitation != null) {
          sendInvitation.click();

          await page.waitFor(3000);

          await page.close();

          res.json({
            statusCode: 200,
            message: "Success",
          });
        } else {
          await page.close();

          res.json({
            statusCode: 500,
            error: "Send Button not found",
          });
        }
      } else {
        await page.close();

        res.json({
          statusCode: 500,
          error: "Add Note Button is not present",
        });
      }
    } else {
      await page.close();

      res.json({
        statusCode: 500,
        error: "All ready a connection or connect button is not present",
      });
    }
  } catch (error) {
    console.error(error);
    res.json({
      statusCode: 500,
      error,
    });
  }
};

module.exports = sendRequest;
