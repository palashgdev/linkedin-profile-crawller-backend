const getVolunteer = async ({ page }) => {
  try {
    const response = await page.$$eval(
      ".pv-profile-section.volunteering-section ul > li.ember-view",
      (nodes) => {
        let data = [];
        for (const node of nodes) {
          const titleElement = node.querySelector(
            ".pv-entity__summary-info h3"
          );
          const title = (titleElement || {}).textContent || null;

          const companyElement = node.querySelector(
            ".pv-entity__summary-info span.pv-entity__secondary-title"
          );
          const company = (companyElement || {}).textContent || null;

          const dateRangeElement = node.querySelector(
            ".pv-entity__date-range span:nth-child(2)"
          );
          const dateRangeText = (dateRangeElement || {}).textContent || null;
          const startDatePart = (dateRangeText || {}).split("–")[0] || null;
          const startDate = (startDatePart || {}).trim() || null;

          const endDatePart = (dateRangeText || {}).split("–")[1] || null;
          const endDateIsPresent =
            (endDatePart || {}).trim().toLowerCase() === "present" || false;
          const endDate =
            endDatePart && !endDateIsPresent ? endDatePart.trim() : "Present";

          const descriptionElement = node.querySelector(
            ".pv-entity__description"
          );
          const description = (descriptionElement || {}).textContent || null;

          data.push({
            title,
            company,
            startDate,
            endDate,
            description,
          });
        }

        return data;
      }
    );

    return {
      statusCode: 200,
      payload: response,
      message: "Success",
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      error,
    };
  }
};

module.exports = getVolunteer;
