const checkLogin = async ({ page, timeout }) => {
  try {
    await page.goto("https://www.linkedin.com/login", {
      waitUntil: "networkidle2",
      timeout: timeout,
    });

    const url = page.url();

    const isLoggedIn = !url.endsWith("/login");

    return isLoggedIn;
  } catch (error) {
    return {
      statusCode: 500,
      error,
    };
  }
};

module.exports = checkLogin;
