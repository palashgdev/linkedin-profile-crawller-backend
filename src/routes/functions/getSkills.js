const getSkills = async ({ page }) => {
  try {
    const response = await page.$$eval(
      ".pv-skill-categories-section ol > .ember-view",
      (nodes) => {
        return nodes.map((node) => {
          const skillName = node.querySelector(
            ".pv-skill-category-entity__name-text"
          );
          const endorsementCount = node.querySelector(
            ".pv-skill-category-entity__endorsement-count"
          );

          return {
            skillName: skillName ? (skillName.textContent || {}).trim() : null,
            endorsementCount: endorsementCount
              ? parseInt((endorsementCount.textContent || {}).trim() || "0")
              : 0,
          };
        });
      }
    );

    return {
      statusCode: 200,
      message: "Success",
      payload: response,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      error,
    };
  }
};

module.exports = getSkills;
