const puppeteer = require("puppeteer");
const getHostname = require("../../lib/getHostName");
const getBlockedHost = require("../../lib/getBlockHost");
const exec = require("child_process").exec;

const page = async ({ headless, timeout, userAgent, sessionCookie }) => {
  try {
    const browser = await puppeteer.launch({
      headless,
      args: [
        // ...(headless ? "---single-process" : "---start-maximized"),
        "--no-sandbox",
        "--disable-setuid-sandbox",
        // "--proxy-server='direct://",
        "--proxy-server=socks5://127.0.0.1:9050",
        "--proxy-bypass-list=*",
        "--disable-dev-shm-usage",
        "--disable-accelerated-2d-canvas",
        "--disable-gpu",
        "--disable-features=site-per-process",
        "--enable-features=NetworkService",
        "--allow-running-insecure-content",
        "--enable-automation",
        "--disable-background-timer-throttling",
        "--disable-backgrounding-occluded-windows",
        "--disable-renderer-backgrounding",
        "--disable-web-security",
        "--autoplay-policy=user-gesture-required",
        "--disable-background-networking",
        "--disable-breakpad",
        "--disable-client-side-phishing-detection",
        "--disable-component-update",
        "--disable-default-apps",
        "--disable-domain-reliability",
        "--disable-extensions",
        "--disable-features=AudioServiceOutOfProcess",
        "--disable-hang-monitor",
        "--disable-ipc-flooding-protection",
        "--disable-notifications",
        "--disable-offer-store-unmasked-wallet-cards",
        "--disable-popup-blocking",
        "--disable-print-preview",
        "--disable-prompt-on-repost",
        "--disable-speech-api",
        "--disable-sync",
        "--disk-cache-size=33554432",
        "--hide-scrollbars",
        "--ignore-gpu-blacklist",
        "--metrics-recording-only",
        "--mute-audio",
        "--no-default-browser-check",
        "--no-first-run",
        "--no-pings",
        "--no-zygote",
        "--password-store=basic",
        "--use-gl=swiftshader",
        "--use-mock-keychain",
      ],
      timeout: timeout,
    });

    const page = await browser.newPage();

    //close the open page
    const firstPage = (await browser.pages())[0];
    await firstPage.close();

    const session = await page.target().createCDPSession();
    await page.setBypassCSP(true);
    await session.send("Page.enable");
    await session.send("Page.setWebLifecycleState", {
      state: "active",
    });

    await page.setRequestInterception(true);

    const blockedResources = [
      "image",
      "media",
      "font",
      "texttrack",
      "object",
      "beacon",
      "csp_report",
      "imageset",
    ];

    const blockedHosts = getBlockedHost();
    const blockedResourcesByHost = ["script", "xhr", "fetch", "document"];

    page.on("request", (req) => {
      if (blockedResources.includes(req.resourceType())) {
        return req.abort();
      }

      const hostname = getHostname(req.url());

      // Block all script requests from all host names
      if (
        blockedResourcesByHost.includes(req.resourceType()) &&
        hostname &&
        blockedHosts[hostname] === true
      ) {
        console.log(
          "blocked script",
          `${req.resourceType()}: ${hostname}: ${req.url()}`
        );
        return req.abort();
      }

      return req.continue();
    });

    page.on("response", (response) => {
      if (response.ok() === false) {
        exec(
          "(echo authenticate '\"\"'; echo signal newnym; echo quit) | nc localhost 9051",
          (error, stdout, stderr) => {
            if (stdout.match(/250/g).length === 3) {
              console.log("Success: The IP Address has been changed.");
            } else {
              console.log(
                "Error: A problem occured while attempting to change the IP Address."
              );
            }
          }
        );
      } else {
        console.log(
          "Success: The Page Response was successful (no need to change the IP Address)."
        );
      }
    });

    await page.setUserAgent(userAgent);

    await page.setViewport({
      width: 1200,
      height: 720,
    });

    await page.setCookie({
      name: "li_at",
      value: sessionCookie,
      domain: ".www.linkedin.com",
    });

    return { page, browser };
  } catch (error) {
    return {
      statusCode: 500,
      error,
    };
  }
};

module.exports = page;
