const getEducation = async ({ page }) => {
  try {
    const response = await page.$$eval(
      "#education-section ul > .ember-view",
      (nodes) => {
        let data = [];
        for (const node of nodes) {
          const schoolNameElement = node.querySelector(
            "h3.pv-entity__school-name"
          );
          const schoolName = (schoolNameElement || {}).textContent || null;

          const degreeNameElement = node.querySelector(
            ".pv-entity__degree-name .pv-entity__comma-item"
          );
          const degreeName = (degreeNameElement || {}).textContent || null;

          const fieldOfStudyElement = node.querySelector(
            ".pv-entity__fos .pv-entity__comma-item"
          );
          const fieldOfStudy = (fieldOfStudyElement || {}).textContent || null;

          const dateRangeElement = node.querySelectorAll(
            ".pv-entity__dates time"
          );

          const startDatePart =
            (dateRangeElement && (dateRangeElement[0] || {}).textContent) ||
            null;
          const startDate = startDatePart || null;

          const endDatePart =
            (dateRangeElement && (dateRangeElement[1] || {}).textContent) ||
            null;
          const endDate = endDatePart || null;

          data.push({
            schoolName,
            degreeName,
            fieldOfStudy,
            startDate,
            endDate,
          });
        }

        return data;
      }
    );

    return {
      statusCode: 200,
      message: "Success",
      payload: response,
    };
  } catch (error) {
    return {
      statusCode: 500,
      error,
    };
  }
};

module.exports = getEducation;
