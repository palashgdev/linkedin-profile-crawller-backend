const getExperience = async ({ page }) => {
  try {
    const response = await page.$$eval(
      "#experience-section ul > .ember-view",
      (nodes) => {
        let data = [];

        for (const node of nodes) {
          const titleElement = node.querySelector("h3");
          const title = (titleElement || {}).textContent || null;

          const employmentTypeElement = node.querySelector(
            "span.pv-entity__secondary-title"
          );
          const employmentType =
            (employmentTypeElement || {}).textContent || null;

          const companyElement = node.querySelector(
            ".pv-entity__secondary-title"
          );
          const companyElementClean =
            companyElement && (companyElement || {}).querySelector("span")
              ? (companyElement || {}).removeChild(
                  companyElement.querySelector("span")
                ) && companyElement
              : companyElement || null;
          const company = (companyElementClean || {}).textContent || null;

          const descriptionElement = node.querySelector(
            ".pv-entity__description"
          );
          const description = (descriptionElement || {}).textContent || null;

          const dateRangeElement = node.querySelector(
            ".pv-entity__date-range span:nth-child(2)"
          );
          const dateRangeText = (dateRangeElement || {}).textContent || null;

          const startDatePart = (dateRangeText || {}).split("–")[0] || null;
          const startDate = (startDatePart || {}).trim() || null;

          const endDatePart = (dateRangeText || {}).split("–")[1] || null;
          const endDateIsPresent =
            (endDatePart || {}).trim().toLowerCase() === "present" || false;
          const endDate =
            endDatePart && !endDateIsPresent ? endDatePart.trim() : "Present";

          const locationElement = node.querySelector(
            ".pv-entity__location span:nth-child(2)"
          );
          const location = (locationElement || {}).textContent || null;

          data.push({
            title,
            company,
            employmentType,
            location,
            startDate,
            endDate,
            endDateIsPresent,
            description,
          });
        }

        return data;
      }
    );

    return {
      statusCode: 200,
      message: "Success",
      payload: response,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      error,
    };
  }
};

module.exports = getExperience;
