const getProfileData = async ({ page }) => {
  try {
    const response = await page.evaluate(() => {
      const profileSection = document.querySelector(".pv-top-card");

      const url = window.location.href;

      const fullNameElement = (profileSection || {}).querySelector(
        ".pv-top-card--list li:first-child"
      );
      const fullName = (fullNameElement || {}).textContent || null;

      const titleElement = (profileSection || {}).querySelector("h2");
      const title = (titleElement || {}).textContent || null;

      const locationElement = (profileSection || {}).querySelector(
        ".pv-top-card--list.pv-top-card--list-bullet.mt1 li:first-child"
      );
      const location = (locationElement || {}).textContent || null;

      const photoElement =
        (profileSection || {}).querySelector(".pv-top-card__photo") ||
        (profileSection || {}).querySelector(".profile-photo-edit__preview");
      const photo = (photoElement || {}).getAttribute("src") || null;

      const descriptionElement = document.querySelector(
        ".pv-about__summary-text"
      );
      const description = (descriptionElement || {}).textContent || null;

      return {
        fullName,
        title,
        location,
        photo,
        description,
        url,
      };
    });

    return {
      statusCode: 200,
      message: "Success",
      payload: response,
    };
  } catch (error) {
    console.error(error);
    return {
      error,
      statusCode: 500,
    };
  }
};

module.exports = getProfileData;
